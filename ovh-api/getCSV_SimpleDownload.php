<?php
//
//  getCSV_SimpleDownload.php
//  telephony-free-tools ( for OVH API )
//
//  Created by Cyrille Georgel on 2016-09-03.
//  Copyright 2016 Cyrille Georgel. All rights reserved.
//

# Use this script if you want to simply download the CSV files.
# If you want to record the calls in your database at the same time,
# use 'getCSV.php' instead.

# For a professional need, have a look at http://bluerocktel.com/

require_once '../_cgGlobal.php';

require_once '../mysql_connect.php';

require_once '../vendor/autoload.php';

use \Ovh\Api;

$ovh = new Api(APP_KEY, APP_SECRET, APP_ENDPOINT, APP_CONSKEY);
				
print  "Welcome " . $ovh->get('/me')['firstname'] . "\n";


# creates the path where we are going to put the csv files:

$dir = getcwd();
$path = $dir . '/csv2';
exec("rm -rf {$path}");
exec("mkdir {$path}");


# sets what we want to get :

$period = date('Y-m-01'); # that's fine for the last available CSV
$services = $ovh -> get('/telephony');
$delay = 15; 

# delay between doc request and download needs to be long if you download many files
# otherwise, the doc is not ready for download


foreach($services as $service)
{	
	print 'For service ' . $service . '';
		
	 try
	 {		 
 		$document = $ovh->get('/telephony/' . $service . '/historyConsumption/' . $period . '/file?extension=csv');
		
 		$document = (object) $document;
		
 		sleep($delay); 
				
		if(!file_exists($path . '/' . $document->filename))
		{
				
			if(!$data = file_get_contents($document->url))
			{
				print ', download of document '. $document->filename . ': error reading file' . "\n";
			}
			else
			{
				if(!file_put_contents($path . '/' . $document->filename, $data))
				{
					print ', download for '. $document->filename . ': error reading file' . "\n";
				}
				else
				{
					print ', download for '. $document->filename . ': success' . "\n";
				}
			}
		}
	} 
	catch (Exception $message)
	{
		print ", no document available for $service.\n";
	}
}

print 'Job is done, thanks and see you soon' . "\n";

?>