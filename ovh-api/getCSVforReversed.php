<?php
//
//  getCSVforReversed.php
//  telephony-free-tools ( for OVH API )
//
//  Created by Cyrille Georgel on 2016-08-06.
//  Copyright 2016 Cyrille Georgel. All rights reserved.
//

# Gets the CSV files for reversed calls ( historyTollfreeConsumption ) in /csv
# Puts the data in a table 'telephonyCallsReversed'
# Doesn't use LOAD DATA INFILE because we need to check for each call if it's not already in the table

# For a professional need, have a look at http://bluerocktel.com/


require_once '../_cgGlobal.php';

require_once '../mysql_connect.php';

require_once '../vendor/autoload.php';

use \Ovh\Api;

$ovh = new Api(APP_KEY, APP_SECRET, APP_ENDPOINT, APP_CONSKEY);
		
print  'Welcome ' . $ovh->get('/me')['firstname'] . "\n";


# creates the path where we are going to put the csv files:

$dir = getcwd();
$path = $dir . '/csv';
exec("rm -rf {$path}");
exec("mkdir {$path}");


# here set the table you want to use:
# fields are : phoneLine, datetime, duration, calledNumber, nature, 
# type, destination, priceWithOutVAT, date, time, designation, idkey, 
# callingNumber, dialedNumber

$tableForTelephonyCallsReversed = 'telephonyCallsReversed';



# sets what we want to get :

$period = date('Y-m-01');
$services = $ovh -> get('/telephony');


foreach($services as $service) 
{
	
	print 'service: ' . $service . "\n";
	
	try
	{
		
		$document = $ovh->get('/telephony/' . $service . '/historyTollfreeConsumption/' . $period . '/document');
		
		$document = (object) $document;
		
		sleep(2); # sometimes the document is not immediately ready for download immediately
		
	
		if(!file_exists( $path . '/' . $document->filename))
		{
			$content = '';
			
			$h = fopen($document->url, 'rb');
			
			while(!feof($h))
			{
				$content .= fread($h, 8192);
			}
			
			fclose($h);
			
			$h = fopen($path . '/' . $document->filename, 'wb');
			
			if(!fwrite($h, $content))
			{
				print 'Download for '. $document->filename . ': error' . "\n";
				
			}
			else
			{
				print 'Download for '. $document->filename . ': success' . "\n";
				
				$h = fopen($path . '/' . $document->filename, 'r');
				
				
				# insert the data in table:
				
				$insert=0;
				
				$sql = "INSERT INTO $tableForTelephonyCallsReversed (idkey, phoneLine, datetime, date, time, duration, callingNumber, operator, priceReversed, type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";		
				$sth = $dbh->prepare($sql);
				
				# second query checks that the call is not is the table already:
				
				$sql1 = "SELECT id FROM $tableForTelephonyCallsReversed WHERE idkey = :idkey";		
				$sth1 = $dbh->prepare($sql1);
				
				while(($data = fgetcsv($h, 0, "\n")) !== FALSE)	
				{	
					for($i=0; $i<count($data); $i++)
					{
						$values = explode(',', $data[$i]);
						
						if(is_numeric($values[0])) # filter titles
						{
							$sth1->bindParam(':idkey', $values[0]); 
							$sth1->execute();
							
							if($sth1->rowCount() == 0) # call is not in the table
							{
								$sth->execute($values);
								$insert++;
							}		
						}
					}
				}
				
				print $insert . ' records inserted in table' . "\n";
				
			}
		}
		else
		{
			print $document->filename . ' was already downloaded' . "\n";
		}

	
	} 
	catch (Exception $message)
	{
		print "no doc available for $service\n";
	}		
}


?>