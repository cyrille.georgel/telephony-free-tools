<?php
//
//  incomingCallsPerDay.php
//  telephony-free-tools
//
//  Created by Cyrille Georgel on 2016-03-31.
//  Copyright 2016 Cyrille Georgel. All rights reserved.
//
// telephony/{billingAccount}/service/{serviceName}/voiceConsumption (works for current month)
// telephony/{billingAccount}/service/{serviceName}/previousVoiceConsumption (works for previous month)

$currentDayD = date('d');

if($currentDayD == 1)
{
	$apiMethod = 'previousVoiceConsumption';
}
else
{
	$apiMethod = 'voiceConsumption';
}


chdir(dirname(__FILE__));

$verbose = 1;
$destinationTable = 'telephonyCallsDirect';

$begTime = time();

include_once '../_cgGlobal.php';

include_once '../_connexionDb.php';

require __DIR__ . '../vendor/autoload.php';

use \Ovh\Api;

$ovh = new Api(APP_KEY, APP_SECRET, APP_ENDPOINT, APP_CONSKEY);

if($verbose == 1)
{
	print  "Welcome " . $ovh->get('/me')['firstname'] . "\n";
}
				


$lastCall = sqlSingleResult("SELECT creationDatetime
	FROM $destinationTable
ORDER BY creationDatetime DESC
LIMIT 0,1");

if(count($lastCall) > 0)
{
	$lastCall = $lastCall['creationDatetime'];
	list($lastCall, $nothing) = explode(' ', $lastCall);
	list($lastCallY, $lastCallM, $lastCallD) = explode('-', $lastCall);
	$beg = date('Y-m-d', mktime(0, 0, 0, $lastCallM, ($lastCallD +1), $lastCallY));	
}
else
{
	$beg = '2016-03-01';
}


if($verbose == 1)
{
	print "beg = $beg\n";
}


$end = date('Y-m-d');

$billingAccounts = $ovh -> get('/telephony');

$i = 0;

foreach($billingAccounts as $billingAccount)
{
	$j = 0;
	
	if($verbose == 1)
	{
		print "billingAccount => $billingAccount\n";
	}
	
	try
	{
		$services = $ovh->get("/telephony/$billingAccount/service");
	
		foreach($services as $service)
		{
			$k = 0;
			
			if($verbose == 1)
			{
				print "service => $service\n";
			}
		   	
			try 
			{
		 		$calls = $ovh->get("/telephony/$billingAccount/service/$service/$apiMethod?creationDatetime.from=$beg&creationDatetime.to=$end");
				
		 		foreach($calls as $callID)
		 		{
					
						
		 			try 
					{
						$callDetails = $ovh->get("/telephony/$billingAccount/service/$service/$apiMethod/$callID");
	
			 			$calling           = $callDetails['calling'];
						$priceWithoutTax   = $callDetails['priceWithoutTax']['value'];
			 			$duration          = $callDetails['duration'];
			 			$type              = $callDetails['destinationType'];
			 			$creationDatetime  = $callDetails['creationDatetime'];
			 			$wayType           = $callDetails['wayType'];
			 			$called            = $callDetails['called'];
		
			 			$qInsert = sqlSimpleQuery("INSERT INTO $destinationTable (billingAccount, phoneLine, callID, calling, priceWithoutTax, duration, destinationType, creationDatetime, wayType, called, insertDate) VALUES ('$billingAccount', '$service', '$callID', '$calling', '$priceWithoutTax', '$duration', '$type', '$creationDatetime', '$wayType', '$called', CURRENT_TIMESTAMP)");
		
			 			if(!$qInsert)
			 			{
							if($verbose == 1)
							{
								print "Alert ! No query !\n";
							}
			 			}
						
			 			else
			 			{
							$i++;
							$j++;
							$k++; 			
						}
						
					} 
					catch(Exception $ex)
					{
						print_r( $ex->getMessage() );
					}
				
				}		
				
			}
		    catch (Exception $ex)
		    {
	   		     print_r( $ex->getMessage() );		
	   	    }
			
			if($verbose == 1)
			{
				print "$k queries for $service\n";
			}
			
		}
		
	}
	catch (Exception $ex)
	{
		print_r( $ex->getMessage() );
	}
	
	if($verbose == 1)
	{
		print "$j queries for $billingAccount\n";
	
		print "$i queries in total so far\n";
	
		print "\n";
	}
	
}

$endTime = time();

$executionTime = $endTime - $begTime;

print "$i queries in total\n
Execution time : $executionTime\n
Job done, thanks and see you soon...\n";



?>