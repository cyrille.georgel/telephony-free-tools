<?php
//
//  getCSV.php
//  telephony-free-tools ( for OVH API )
//
//  Created by Cyrille Georgel on 2016-09.03.
//  Copyright 2016 Cyrille Georgel. All rights reserved.
//

# Use this script if you want to simply download the CSV files
# and record the calls in your database at the same time.
# If you want to simply download the files, use 'getCSV_SimpleDownload.php' instead.

# For a professional need, have a look at http://bluerocktel.com/

require_once '../_cgGlobal.php';

require_once '../mysql_connect.php';

require_once'../vendor/autoload.php';

use \Ovh\Api;

$ovh = new Api(APP_KEY, APP_SECRET, APP_ENDPOINT, APP_CONSKEY);
				
print  "Welcome " . $ovh->get('/me')['firstname'] . "\n";


# creates the path where we are going to put the csv files:

$dir = getcwd();
$path = $dir . '/csv2';
exec("rm -rf {$path}");
exec("mkdir {$path}");


# here set the table you want to use:
# fields are : phoneLine, datetime, duration, calledNumber, nature, 
# type, destination, priceWithOutVAT, date, time, designation, idkey, 
# callingNumber, dialedNumber

$tableForTelephonyCalls = 'telephonyCalls';


# sets what we want to get :

$period = date('Y-m-01'); # that's fine for the last available CSV
$services = $ovh -> get('/telephony');
$delay = 15; 

# delay between doc request and download needs to be long if you download many files
# otherwise, the doc is not ready for download


foreach($services as $service)
{	
	print 'For service ' . $service . '';
		
	 try
	 {
		 
 		$document = $ovh->get('/telephony/' . $service . '/historyConsumption/' . $period . '/file?extension=csv');
		
 		$document = (object) $document;
		
 		sleep($delay); 
				
		if(!file_exists($path . '/' . $document->filename))
		{
				
			if(!$data = file_get_contents($document->url))
			{
				print ', download of document '. $document->filename . ': error reading file' . "\n";
			}
			else
			{
				if(!file_put_contents($path . '/' . $document->filename, $data))
				{
					print ', download for '. $document->filename . ': error reading file' . "\n";
				}
				else
				{
					print ', download for '. $document->filename . ': success' . "\n";
					
					# sends the content of the file to the database:
					
					$insert = 0;
					
					$sth = $dbh->prepare("INSERT INTO $tableForTelephonyCalls (phoneLine, datetime, duration, calledNumber, nature, type, destination, priceWithOutVAT, date, time, designation, idkey, callingNumber, dialedNumber) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
							
					$sth1 = $dbh->prepare("SELECT idkey FROM $tableForTelephonyCalls WHERE idkey = :idkey"); # checks if the call is already in the table or not
					
					$lines = explode("\n", $data); # I was using fgetcsv but I think this is much simplier:
					
					foreach($lines as $line)
					{
						$line = str_replace('+', '00', $line); # because I like to have 00331... instead of +331...
						$values = explode(',', $line);
						
						if(is_numeric($values[11])) # filter titles
						{
							$sth1->bindParam(':idkey', $values[11]); 
							$sth1->execute();
							
							if($sth1->rowCount() == 0) # call is not in the table
							{
								$sth->execute($values);
								$insert++;
							}	
						}
					}
					
					print $insert . ' records inserted in table from file ' . $document->filename . "\n";
				}
			}
		}
	} 
	catch (Exception $message)
	{
		print ", no document available for $service.\n";
	}
}

print 'Job is done, thanks and see you soon' . "\n";

?>