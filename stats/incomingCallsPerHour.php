<?php
//
//  incomingCallsPerHour.php
//  telephony-free-tools
//
//  Created by Cyrille Georgel on 2016-03-31.
//  Copyright 2016 Cyrille Georgel. All rights reserved.
//


/* At what moments of the day do you receive calls ? */
	
include_once '../_cgGlobal.php';

include_once '../_connexionDb.php';

$table = 'telephonyCallsDirect';



$timeSlots = array(
	'00' => 0,
	'01' => 0,
	'02' => 0,
	'03' => 0,
	'04' => 0,
	'05' => 0,
	'06' => 0,
	'07' => 0,
	'08' => 0,
	'09' => 0,
	'10' => 0,
	'11' => 0,
	'12' => 0,
	'13' => 0,
	'14' => 0,
	'15' => 0,
	'16' => 0,
	'17' => 0,
	'18' => 0,
	'19' => 0,
	'20' => 0,
	'21' => 0,
	'22' => 0,
	'23' => 0,
);


foreach($timeSlots as $key => $value)
{
	$calls = sqlManyResults("SELECT id
		FROM $table
	WHERE wayType = 'incoming'
	AND timeSlot = '$key'");
	
	$timeSlots[$key] = count($calls);
		
}


arsort($timeSlots);

print_r($timeSlots);

?>