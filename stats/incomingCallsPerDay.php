<?php
//
//  incomingCallsPerDay.php
//  telephony-free-tools
//
//  Created by Cyrille Georgel on 2016-03-31.
//  Copyright 2016 Cyrille Georgel. All rights reserved.
//

/* Wich day of the week do you receive calls ? */
	
include_once '../_cgGlobal.php';

include_once '../_connexionDb.php';

$table = 'telephonyCallsDirect';



$callsPerDay = array(
	'Mon' => 0,
	'Tue' => 0,
	'Wed' => 0, 
	'Thu' => 0, 
	'Fri' => 0, 
	'Sat' => 0, 
	'Sun' => 0
);



foreach($callsPerDay as $key => $value)
{
	$calls = sqlManyResults("SELECT id
		FROM $table
	WHERE wayType = 'incoming'
	AND weekDay = '$key'");
	
	$callsPerDay[$key] = count($calls);
		
}

arsort($callsPerDay);

print_r($callsPerDay);

?>