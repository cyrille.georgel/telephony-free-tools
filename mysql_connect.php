<?php

include_once '_cgGlobal.php';

try
{
	$dbh = new PDO(DB_SERVER_DATABASE, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
	$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
}
catch(PDOException $e)
{
	echo $e->getMessage();
}

?>