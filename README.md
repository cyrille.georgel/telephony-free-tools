## Synopsis

Just some PHP/MySQL tools for telephony management with **OVH API** and the  **[PHP Wrapper for OVH API](https://github.com/ovh/php-ovh)**. You should download and install the [PHP Wrapper for OVH API](https://github.com/ovh/php-ovh) first.

**For a professional need, have a look at http://bluerocktel.com/**