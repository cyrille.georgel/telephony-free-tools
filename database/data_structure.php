<?php
//
//  data_structure.php
//  telephony-free-tools
//
//  Created by Cyrille Georgel on 2016-04-28.
//  Copyright 2016 Cyrille Georgel. All rights reserved.
//

//
// Well, I needed to have my database structure in an Excel file...
// This creates a simple CSV file with the structure of your database (data_dictionary.csv)
// 
	
include_once '_cgGlobal.php';

include_once '_connexionDb.php';


$dir = getcwd();
$path = $dir . '/data_structure/';
exec("rm -rf {$path}");
exec("mkdir {$path}");

$filename = "data_dictionary.csv";

$data = array();

$tables = mysqli_query($link,"SHOW TABLES");

while ($row = $tables->fetch_row())
{
	$table = $row[0];
	
	print "\n\n$table:\n";
	
    $tableDetails = sqlManyResults("SHOW COLUMNS FROM $table");
	
	foreach($tableDetails as $detail)
	{
		$field_name = $detail['Field'];
		$field_type = $detail['Type'];
		$field_null = $detail['Null'];
		$field_key = $detail['Key'];
		$field_default = $detail['Default'];
		$field_extra = $detail['Extra'];
		
		print "$field_name => $field_type => $field_null => $field_key => $field_default => $field_extra\n";
		
		$newData = array($table,$field_name,$field_type,$field_null,$field_key,$field_default,$field_extra);
	
		array_push($data, $newData);
		
	}

}

$fp = fopen("$path"."$filename", 'w');

foreach($data as $fields)
{
	fputcsv($fp, $fields);
}

fclose($fp);
	
?>